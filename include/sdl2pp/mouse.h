#ifndef SDL2PP_MOUSE
#define SDL2PP_MOUSE

#include <glm/glm.hpp>

#include <bitset>

namespace SDL {
class Mouse {
protected:
    struct State {
    private:
        static const uint32_t NUM_BUTTONS = sizeof(unsigned int) * 8;

    public:
        glm::ivec2 const position;
        std::bitset<NUM_BUTTONS> const buttons;
    };

public:
    [[nodiscard]] static auto state() -> State;
};
} // namespace SDL
#endif // SDL2PP_MOUSE
