#ifndef SDL2PP_FLAG
#define SDL2PP_FLAG

#include <cstdint>

namespace SDL {
namespace Flag {

using Type = uint32_t;

template<typename FlagType>
constexpr auto operator|(FlagType const & first, FlagType const & second)
    -> FlagType {
    return static_cast<FlagType>(static_cast<Type>(first)
                                 | static_cast<Type>(second));
}

} // namespace Flag
} // namespace SDL

#endif // SDL2PP_FLAG
