#ifndef SDL2PP_TEXTUE
#define SDL2PP_TEXTUE

#include "flag.h"

#include <SDL_render.h>

#include <memory>
#include <utility>

class SDL_Texture;

namespace SDL {
class Texture {
private:
    struct TextureDeleter {
    private:
        bool const _destroy;

    public:
        explicit TextureDeleter(bool destroy);
        auto operator()(SDL_Texture * texture) const -> void;
    };

    std::unique_ptr<SDL_Texture, TextureDeleter> const texture;

    struct ExtraArgs {
        SDL_Renderer * renderer;
    };

public:
    struct Of {
        SDL_Texture * texture;
    };
    explicit Texture(Of const & args);

    [[nodiscard]] auto get() const -> SDL_Texture *;

    enum Access : Flag::Type;

    struct Args {
        int pixelFormat; // SDL_PIXELFORMAT_RGBA8888,
        int access;      // SDL_TEXTUREACCESS_TARGET,
        int w;
        int h; // size.x,
    };
    struct Args_ : public Args, public ExtraArgs {};

    explicit Texture(Args_ const & args);

    // extern DECLSPEC int SDL_QueryTexture(SDL_Texture * texture,
    //                                              uint32_t * format, int
    //                                              *access, int *w, int *h);
    //
    // extern DECLSPEC int SDL_SetTextureColorMod(SDL_Texture * texture,
    //                                                    uint8_t r, uint8_t g,
    //                                                    uint8_t b);
    //
    //
    // extern DECLSPEC int SDL_GetTextureColorMod(SDL_Texture * texture,
    //                                                    uint8_t * r, uint8_t *
    //                                                    g, uint8_t * b);
    //
    // extern DECLSPEC int SDL_SetTextureAlphaMod(SDL_Texture * texture,
    //                                                    uint8_t alpha);
    //
    // extern DECLSPEC int SDL_GetTextureAlphaMod(SDL_Texture * texture,
    //                                                    uint8_t * alpha);
    //
    // extern DECLSPEC int SDL_SetTextureBlendMode(SDL_Texture * texture,
    //                                                     SDL_BlendMode
    //                                                     blendMode);
    //
    // extern DECLSPEC int SDL_GetTextureBlendMode(SDL_Texture * texture,
    //                                                     SDL_BlendMode
    //                                                     *blendMode);
    //
    // extern DECLSPEC int SDL_UpdateTexture(SDL_Texture * texture,
    //                                               const SDL_Rect * rect,
    //                                               const void *pixels, int
    //                                               pitch);
    //
    // extern DECLSPEC int SDL_UpdateYUVTexture(SDL_Texture * texture,
    //                                                  const SDL_Rect * rect,
    //                                                  const uint8_t *Yplane,
    //                                                  int Ypitch, const
    //                                                  uint8_t *Uplane, int
    //                                                  Upitch, const uint8_t
    //                                                  *Vplane, int Vpitch);
    //
    // extern DECLSPEC int SDL_LockTexture(SDL_Texture * texture,
    //                                             const SDL_Rect * rect,
    //                                             void **pixels, int *pitch);
    //
    // extern DECLSPEC void SDL_UnlockTexture(SDL_Texture * texture);
    //
    // extern DECLSPEC int SDL_GL_BindTexture(SDL_Texture *texture, float *texw,
    // float *texh);
    //
    // extern DECLSPEC int SDL_GL_UnbindTexture(SDL_Texture *texture);
};

enum Texture::Access : Flag::Type {
    STATIC = SDL_TEXTUREACCESS_STATIC,
    STREAMING = SDL_TEXTUREACCESS_STREAMING,
    TARGET = SDL_TEXTUREACCESS_TARGET,
};
} // namespace SDL

#endif
