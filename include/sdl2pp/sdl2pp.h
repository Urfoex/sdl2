#ifndef SDL2PP_SDL2PP
#define SDL2PP_SDL2PP

#include "flag.h"

#include <SDL.h>

namespace SDL {

namespace Init {
enum Flags : Flag::Type;
} // namespace Init

class SDL {
public:
    SDL();
    ~SDL();

    auto initSubSystem(Init::Flags flags) -> void;
    auto quitSubSystem(Init::Flags flags) -> void;
    auto wasInit(Init::Flags flags) -> bool;
};

enum Init::Flags : Flag::Type {
    TIMER = SDL_INIT_TIMER,
    AUDIO = SDL_INIT_AUDIO,
    VIDEO = SDL_INIT_VIDEO,
    JOYSTICK = SDL_INIT_JOYSTICK,
    HAPTIC = SDL_INIT_HAPTIC,
    GAMECONTROLLER = SDL_INIT_GAMECONTROLLER,
    EVENTS = SDL_INIT_EVENTS,
    NOPARACHUTE = SDL_INIT_NOPARACHUTE,
    EVERYTHING = SDL_INIT_EVERYTHING
};

constexpr auto operator|(Init::Flags const & first, Init::Flags const & second)
    -> Init::Flags {
    return Flag::operator|(first, second);
}

} // namespace SDL

#endif // SDL2PP_SDL2PP
