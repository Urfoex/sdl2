#ifndef SDL2PP_WITH
#define SDL2PP_WITH

#include <functional>

template<typename... ResourceTypes>
constexpr auto with(ResourceTypes &&... resources) {
    return
        [&resources...](
            std::function<auto(ResourceTypes && ...)->void> callback) -> void {
            callback(std::forward<ResourceTypes>(resources)...);
        };
}

#endif // SDL2PP_WITH
