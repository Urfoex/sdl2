#ifndef SDL2PP_VISITORS
#define SDL2PP_VISITORS

template<typename... Ts>
struct Visitors : Ts... {
    using Ts::operator()...;
};

// FIXME: try without again later with clang update
template<typename... Ts>
Visitors(Ts...) -> Visitors<Ts...>;

#endif // SDL2PP_VISITORS
