#ifndef SDL2PP_ERROR
#define SDL2PP_ERROR

#include <functional>
#include <stdexcept>

namespace SDL {

class Error : public std::runtime_error {
public:
    Error();
    template<typename Type>
    static constexpr auto
    validate(Type const && instance,
             std::function<auto(Type const & instance)->bool> const & check)
        -> Type;

    template<typename Type>
    static constexpr auto isNullptr(Type const * instance) -> bool;

    template<typename Type>
    static constexpr auto isNotZero(Type const & instance) -> bool;
};

template<typename Type>
constexpr auto
Error::validate(Type const && instance,
                std::function<auto(Type const & instance)->bool> const & check)
    -> Type {
    if (check(instance)) {
        throw Error();
    }
    return std::forward<Type const>(instance);
}

template<typename Type>
constexpr auto Error::isNullptr(Type const * const instance) -> bool {
    return instance == nullptr;
}

template<typename Type>
constexpr auto Error::isNotZero(Type const & instance) -> bool {
    return instance != Type{0};
}

} // namespace SDL

#endif
