#ifndef SDL2PP_TIMER
#define SDL2PP_TIMER

#include <chrono>

namespace SDL {
namespace Timer {

auto delay(std::chrono::milliseconds ms) -> void;

} // namespace Timer
} // namespace SDL

#endif // SDL2PP_TIMER
