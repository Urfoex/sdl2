#ifndef SDL2PP_RENDERER
#define SDL2PP_RENDERER

#include "color.h"
#include "flag.h"
#include "point.h"
#include "rect.h"
#include "texture.h"

#include <SDL_render.h>

#include <cstdint>
#include <memory>
#include <optional>
#include <vector>

class SDL_Renderer;

namespace SDL {

class Renderer {
private:
    struct RendererDeleter {
        auto operator()(SDL_Renderer * renderer) -> void;
    };

    std::unique_ptr<SDL_Renderer, RendererDeleter> const renderer;

    struct ExtraArgs {
        SDL_Window * window;
    };

public:
    enum Flags : Flag::Type;
    struct Args {
        std::optional<int> index = std::nullopt;
        Flags flags;
    };
    struct Args_ : public Args, public ExtraArgs {};
    explicit Renderer(Args_ const & args);
    auto present() const -> void;
    auto setDrawColor(Color const & color) const -> void;
    auto clear() const -> void;

    [[nodiscard]] auto getTarget() const -> std::shared_ptr<Texture>;
    auto setTarget(std::shared_ptr<Texture> const & texture) const -> void;

    [[nodiscard]] auto createTexture(Texture::Args const & args) const
        -> std::shared_ptr<Texture>;

    auto copy(std::shared_ptr<Texture> const & texture,
              std::optional<Rect> const & src,
              std::optional<Rect> const & dst) const -> void;

    auto drawLine(Point const & start, Point const & end) const -> void;
    auto drawPoint(Point const & point) -> void;
    auto drawRect(Rect const & rect) -> void;
    auto drawLines(std::vector<Point> const & points) -> void;
    auto fillRect(Rect const & rect) -> void;

public:
    // static auto GetNumivers();

    // static auto GetiverInfo(int index);

    // auto GetInfo();

    // auto GetOutputSize();

    // auto CreateTexture(uint32_t format, int access, int w, int h);

    // auto targetSupported();

    // void drawPoint(glm::ivec2 p);
    // void drawPoint(glm::ivec2 & p);
    // void drawPoint(Point p);
    // void drawPoint(Point & p);

    // void drawPoint(int x, int y);

    // void drawPoints(std::vector<Point> & points);

    // void drawLine(glm::ivec2 p1, glm::ivec2 p2);
    // void drawLine(glm::ivec2 & p1, glm::ivec2 & p2);
    // void drawLine(Point p1, Point p2);
    // void drawLine(Point & p1, Point & p2);

    // void drawLine(int x1, int y1, int x2, int y2);

    // void drawLines(std::vector<Point> & points);

    // void drawRect(Rect & rect);

    // void drawRects(std::vector<Rect> & rects);

    // void fillRect(Rect & rect);

    // void fillRects(std::vector<Rect> & rects);
    // void SetDrawColor(glm::i8vec4 color);
    // void SetDrawColor(glm::i8vec4 & color);
    // void SetDrawColor(uint8_t r, uint8_t g, uint8_t b, uint8_t a);

    // static SDL_Renderer * SDL_CreateSoftwareRenderer(SDL_Surface * surface);
    //
    //
    // extern DECLSPEC SDL_Texture * SDL_CreateTextureFromSurface(SDL_Renderer *
    // renderer, SDL_Surface * surface);
    //
    // extern DECLSPEC int SDL_SetRenderTarget(SDL_Renderer *renderer,
    //                                                 SDL_Texture *texture);
    //
    // extern DECLSPEC SDL_Texture * SDL_GetRenderTarget(SDL_Renderer
    // *renderer);
    //
    // extern DECLSPEC int SDL_RenderSetLogicalSize(SDL_Renderer * renderer, int
    // w, int h);
    //
    // extern DECLSPEC void SDL_RenderGetLogicalSize(SDL_Renderer * renderer,
    // int *w, int *h);
    //
    // extern DECLSPEC int SDL_RenderSetViewport(SDL_Renderer * renderer,
    //                                                   const SDL_Rect * rect);
    //
    // extern DECLSPEC void SDL_RenderGetViewport(SDL_Renderer * renderer,
    //                                                    SDL_Rect * rect);
    //
    // extern DECLSPEC int SDL_RenderSetClipRect(SDL_Renderer * renderer,
    //                                                   const SDL_Rect * rect);
    //
    // extern DECLSPEC void SDL_RenderGetClipRect(SDL_Renderer * renderer,
    //                                                    SDL_Rect * rect);
    //
    // extern DECLSPEC int SDL_RenderSetScale(SDL_Renderer * renderer,
    //                                                float scaleX, float
    //                                                scaleY);
    //
    // extern DECLSPEC void SDL_RenderGetScale(SDL_Renderer * renderer,
    //                                                float *scaleX, float
    //                                                *scaleY);
    //
    // extern DECLSPEC int SDL_GetRenderDrawColor(SDL_Renderer * renderer,
    //                                            uint8_t * r, uint8_t * g,
    //                                            uint8_t * b, uint8_t * a);
    //
    // extern DECLSPEC int SDL_SetRenderDrawBlendMode(SDL_Renderer * renderer,
    //                                                        SDL_BlendMode
    //                                                        blendMode);
    //
    // extern DECLSPEC int SDL_GetRenderDrawBlendMode(SDL_Renderer * renderer,
    //                                                        SDL_BlendMode
    //                                                        *blendMode);
    //

    // extern DECLSPEC int SDL_RenderCopy(SDL_Renderer * renderer,
    //                                            SDL_Texture * texture,
    //                                            const SDL_Rect * srcrect,
    //                                            const SDL_Rect * dstrect);
    //
    // extern DECLSPEC int SDL_RenderCopyEx(SDL_Renderer * renderer,
    //                                            SDL_Texture * texture,
    //                                            const SDL_Rect * srcrect,
    //                                            const SDL_Rect * dstrect,
    //                                            const double angle,
    //                                            const SDL_Point *center,
    //                                            const SDL_RendererFlip flip);
    //
    // extern DECLSPEC int SDL_RenderReadPixels(SDL_Renderer * renderer,
    //                                                  const SDL_Rect * rect,
    //                                                  uint32_t format,
    //                                                  void *pixels, int
    //                                                  pitch);
    //
};

enum Renderer::Flags : Flag::Type {
    NONE = 0,
    SOFTWARE = SDL_RENDERER_SOFTWARE,
    ACCELERATED = SDL_RENDERER_ACCELERATED,
    PRESENTVSYNC = SDL_RENDERER_PRESENTVSYNC,
    TARGETTEXTURE = SDL_RENDERER_TARGETTEXTURE,
};

constexpr auto operator|(Renderer::Flags const & first,
                         Renderer::Flags const & second) -> Renderer::Flags {
    return Flag::operator|<Renderer::Flags>(first, second);
}

} // namespace SDL

#endif // SDL2PP_RENDERER
