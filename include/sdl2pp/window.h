#ifndef SDL2PP_WINDOW
#define SDL2PP_WINDOW

#include "flag.h"
#include "renderer.h"

#include <SDL_render.h>

#include <cstdint>
#include <memory>
#include <string_view>

struct SDL_Window;

namespace SDL {

class Window {
public:
    enum Flags : Flag::Type;
    enum Pos : Flag::Type;

private:
    struct WindowDeleter {
        auto operator()(SDL_Window * window) -> void;
    };
    std::unique_ptr<SDL_Window, WindowDeleter> window;

public:
    struct Args {
        std::string_view const & title;
        int x;
        int y;
        int w;
        int h;
        Flags flags;
    };
    explicit Window(Args const & args);
    auto createRenderer(Renderer::Args args) -> Renderer;
    auto createSharedRenderer(Renderer::Args args) -> std::shared_ptr<Renderer>;
};

enum Window::Flags : Flag::Type {
    NONE = 0,
    FULLSCREEN = SDL_WINDOW_FULLSCREEN,
    OPENGL = SDL_WINDOW_OPENGL,
    SHOWN = SDL_WINDOW_SHOWN,
    HIDDEN = SDL_WINDOW_HIDDEN,
    BORDERLESS = SDL_WINDOW_BORDERLESS,
    RESIZABLE = SDL_WINDOW_RESIZABLE,
    MINIMIZED = SDL_WINDOW_MINIMIZED,
    MAXIMIZED = SDL_WINDOW_MAXIMIZED,
#ifdef SDL_WINDOW_MOUSE_GRABBED
    MOUSE_GRABBED = SDL_WINDOW_MOUSE_GRABBED,
#endif
    INPUT_FOCUS = SDL_WINDOW_INPUT_FOCUS,
    MOUSE_FOCUS = SDL_WINDOW_MOUSE_FOCUS,
    FULLSCREEN_DESKTOP = SDL_WINDOW_FULLSCREEN_DESKTOP,
    FOREIGN = SDL_WINDOW_FOREIGN,
    ALLOW_HIGHDPI = SDL_WINDOW_ALLOW_HIGHDPI,
    MOUSE_CAPTURE = SDL_WINDOW_MOUSE_CAPTURE,
    ALWAYS_ON_TOP = SDL_WINDOW_ALWAYS_ON_TOP,
    SKIP_TASKBAR = SDL_WINDOW_SKIP_TASKBAR,
    UTILITY = SDL_WINDOW_UTILITY,
    TOOLTIP = SDL_WINDOW_TOOLTIP,
    POPUP_MENU = SDL_WINDOW_POPUP_MENU,
#ifdef SDL_WINDOW_KEYBOARD_GRABBED
    KEYBOARD_GRABBED = SDL_WINDOW_KEYBOARD_GRABBED,
#endif
    VULKAN = SDL_WINDOW_VULKAN,
#ifdef SDL_WINDOW_METAL
    METAL = SDL_WINDOW_METAL,
#endif
    INPUT_GRABBED = SDL_WINDOW_INPUT_GRABBED,
};

constexpr auto operator|(Window::Flags const & first,
                         Window::Flags const & second) -> Window::Flags {
    return Flag::operator|(first, second);
}

enum Window::Pos : Flag::Type {
    CENTERED = SDL_WINDOWPOS_CENTERED,
    UNDEFINED = SDL_WINDOWPOS_UNDEFINED,
};

} // namespace SDL

#endif // SDL2PP_WINDOW
