#ifndef SDL2PP_RECT
#define SDL2PP_RECT

#include <SDL_rect.h>

namespace SDL {
using Rect = SDL_Rect;
} // namespace SDL

#endif
