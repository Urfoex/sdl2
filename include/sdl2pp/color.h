#ifndef SDL2PP_COLOR
#define SDL2PP_COLOR

#include <cstdint>

namespace SDL {
class Color {
public:
    uint8_t const r;
    uint8_t const g;
    uint8_t const b;
    uint8_t const a;
};
} // namespace SDL

#endif // SDL2PP_COLOR
