#ifndef SDL2PP_GLM2SDL2PP
#define SDL2PP_GLM2SDL2PP

#include "point.h"

#include <glm/fwd.hpp>

#include <vector>

namespace GLM {

auto toPoint(glm::dvec2 const & vec) -> SDL::Point;
auto toPoints(std::vector<glm::dvec2> const & points)
    -> std::vector<SDL::Point>;

} // namespace GLM

#endif // SDL2PP_GLM2SDL2PP
