#ifndef SDL2PP_EVENT
#define SDL2PP_EVENT

#include "flag.h"
#include "visitors.h"

#include <SDL_events.h>

#include <stdexcept>
#include <variant>

namespace SDL {
namespace Events {
using Common = SDL_CommonEvent;
using Display = SDL_DisplayEvent;
using Window = SDL_WindowEvent;
using Keyboard = SDL_KeyboardEvent;

namespace Text {
using Editing = SDL_TextEditingEvent;
using Input = SDL_TextInputEvent;
} // namespace Text

namespace Mouse {
using Motion = SDL_MouseMotionEvent;
using Button = SDL_MouseButtonEvent;
using Wheel = SDL_MouseWheelEvent;
} // namespace Mouse

namespace Joy {
using Axis = SDL_JoyAxisEvent;
using Ball = SDL_JoyBallEvent;
using Hat = SDL_JoyHatEvent;
using Button = SDL_JoyButtonEvent;
using Device = SDL_JoyDeviceEvent;
} // namespace Joy

namespace Controller {
using Axis = SDL_ControllerAxisEvent;
using Button = SDL_ControllerButtonEvent;
using Device = SDL_ControllerDeviceEvent;
#ifdef SDL_ControllerTouchpadEvent
using Touchpad = SDL_ControllerTouchpadEvent;
#endif // SDL_ControllerTouchpadEvent
#ifdef SDL_ControllerSensorEvent
using Sensor = SDL_ControllerSensorEvent;
#endif // SDL_ControllerSensorEvent
} // namespace Controller

using AudioDevice = SDL_AudioDeviceEvent;
using Sensor = SDL_SensorEvent;
using Quit = SDL_QuitEvent;
using User = SDL_UserEvent;
using SysWM = SDL_SysWMEvent;
using TouchFinger = SDL_TouchFingerEvent;

namespace Gesture {
using Multi = SDL_MultiGestureEvent;
using Dollar = SDL_DollarGestureEvent;
} // namespace Gesture

using Drop = SDL_DropEvent;

enum Type : Flag::Type;

[[maybe_unused]] constexpr auto PRESSED = SDL_PRESSED;
[[maybe_unused]] constexpr auto RELEASED = SDL_RELEASED;

enum KeyCode : Flag::Type;

namespace MouseButton {

[[maybe_unused]] constexpr auto LEFT = SDL_BUTTON_LEFT;
[[maybe_unused]] constexpr auto MIDDLE = SDL_BUTTON_MIDDLE;
[[maybe_unused]] constexpr auto RIGHT = SDL_BUTTON_RIGHT;
[[maybe_unused]] constexpr auto X1 = SDL_BUTTON_X1;
[[maybe_unused]] constexpr auto X2 = SDL_BUTTON_X2;
[[maybe_unused]] constexpr auto LMASK = SDL_BUTTON_LMASK;
[[maybe_unused]] constexpr auto MMASK = SDL_BUTTON_MMASK;
[[maybe_unused]] constexpr auto RMASK = SDL_BUTTON_RMASK;
[[maybe_unused]] constexpr auto X1MASK = SDL_BUTTON_X1MASK;
[[maybe_unused]] constexpr auto X2MASK = SDL_BUTTON_X2MASK;

} // namespace MouseButton

} // namespace Events

using Event = std::variant<Events::Common,
                           Events::Display,
                           Events::Window,
                           Events::Keyboard,
                           Events::Text::Editing,
                           Events::Text::Input,
                           Events::Mouse::Motion,
                           Events::Mouse::Button,
                           Events::Mouse::Wheel,
                           Events::Joy::Axis,
                           Events::Joy::Ball,
                           Events::Joy::Hat,
                           Events::Joy::Button,
                           Events::Joy::Device,
                           Events::Controller::Axis,
                           Events::Controller::Button,
                           Events::Controller::Device,
#ifdef SDL_ControllerTouchpadEvent
                           Events::Controller::Touchpad,
#endif // SDL_ControllerTouchpadEvent
#ifdef SDL_ControllerSensorEvent
                           Events::Controller::Sensor,
#endif // SDL_ControllerSensorEvent
                           Events::AudioDevice,
                           Events::Sensor,
                           Events::Quit,
                           Events::User,
                           Events::SysWM,
                           Events::TouchFinger,
                           Events::Gesture::Multi,
                           Events::Gesture::Dollar,
                           Events::Drop>;

class UnknownEvent : public std::runtime_error {
public:
    explicit UnknownEvent(uint32_t eventType);
};

namespace Events {
constexpr auto convert(SDL_Event const & event) -> Event;
} // namespace Events

class EventHandler {
private:
    SDL_Event _event;
    Event event;

    auto poll() -> bool;

public:
    template<typename... Types>
    auto handleEvents(Visitors<Types...> const & visitors) -> void;
};

template<typename... Types>
auto EventHandler::handleEvents(Visitors<Types...> const & visitors) -> void {
    while (this->poll()) {
        std::visit(visitors, this->event);
    }
}

enum Events::Type : Flag::Type {
    FIRSTEVENT = SDL_FIRSTEVENT,
    QUIT = SDL_QUIT,
    APP_TERMINATING = SDL_APP_TERMINATING,
    APP_LOWMEMORY = SDL_APP_LOWMEMORY,
    APP_WILLENTERBACKGROUND = SDL_APP_WILLENTERBACKGROUND,
    APP_DIDENTERBACKGROUND = SDL_APP_DIDENTERBACKGROUND,
    APP_WILLENTERFOREGROUND = SDL_APP_WILLENTERFOREGROUND,
    APP_DIDENTERFOREGROUND = SDL_APP_DIDENTERFOREGROUND,
#ifdef SDL_LOCALECHANGED
    LOCALECHANGED = SDL_LOCALECHANGED,
#endif // SDL_LOCALECHANGED
    DISPLAYEVENT = SDL_DISPLAYEVENT,
    WINDOWEVENT = SDL_WINDOWEVENT,
    SYSWMEVENT = SDL_SYSWMEVENT,
    KEYDOWN = SDL_KEYDOWN,
    KEYUP = SDL_KEYUP,
    TEXTEDITING = SDL_TEXTEDITING,
    TEXTINPUT = SDL_TEXTINPUT,
    KEYMAPCHANGED = SDL_KEYMAPCHANGED,
    MOUSEMOTION = SDL_MOUSEMOTION,
    MOUSEBUTTONDOWN = SDL_MOUSEBUTTONDOWN,
    MOUSEBUTTONUP = SDL_MOUSEBUTTONUP,
    MOUSEWHEEL = SDL_MOUSEWHEEL,
    JOYAXISMOTION = SDL_JOYAXISMOTION,
    JOYBALLMOTION = SDL_JOYBALLMOTION,
    JOYHATMOTION = SDL_JOYHATMOTION,
    JOYBUTTONDOWN = SDL_JOYBUTTONDOWN,
    JOYBUTTONUP = SDL_JOYBUTTONUP,
    JOYDEVICEADDED = SDL_JOYDEVICEADDED,
    JOYDEVICEREMOVED = SDL_JOYDEVICEREMOVED,
    CONTROLLERAXISMOTION = SDL_CONTROLLERAXISMOTION,
    CONTROLLERBUTTONDOWN = SDL_CONTROLLERBUTTONDOWN,
    CONTROLLERBUTTONUP = SDL_CONTROLLERBUTTONUP,
    CONTROLLERDEVICEADDED = SDL_CONTROLLERDEVICEADDED,
    CONTROLLERDEVICEREMOVED = SDL_CONTROLLERDEVICEREMOVED,
    CONTROLLERDEVICEREMAPPED = SDL_CONTROLLERDEVICEREMAPPED,
#ifdef SDL_CONTROLLERTOUCHPADDOWN
    CONTROLLERTOUCHPADDOWN = SDL_CONTROLLERTOUCHPADDOWN,
#endif // SDL_CONTROLLERTOUCHPADDOWN
#ifdef SDL_CONTROLLERTOUCHPADMOTION
    CONTROLLERTOUCHPADMOTION = SDL_CONTROLLERTOUCHPADMOTION,
#endif // SDL_CONTROLLERTOUCHPADMOTION
#ifdef SDL_CONTROLLERTOUCHPADUP
    CONTROLLERTOUCHPADUP = SDL_CONTROLLERTOUCHPADUP,
#endif // SDL_CONTROLLERTOUCHPADUP
#ifdef SDL_CONTROLLERSENSORUPDATE
    CONTROLLERSENSORUPDATE = SDL_CONTROLLERSENSORUPDATE,
#endif // SDL_CONTROLLERSENSORUPDATE
    FINGERDOWN = SDL_FINGERDOWN,
    FINGERUP = SDL_FINGERUP,
    FINGERMOTION = SDL_FINGERMOTION,
    DOLLARGESTURE = SDL_DOLLARGESTURE,
    DOLLARRECORD = SDL_DOLLARRECORD,
    MULTIGESTURE = SDL_MULTIGESTURE,
    CLIPBOARDUPDATE = SDL_CLIPBOARDUPDATE,
    DROPFILE = SDL_DROPFILE,
    DROPTEXT = SDL_DROPTEXT,
    DROPBEGIN = SDL_DROPBEGIN,
    DROPCOMPLETE = SDL_DROPCOMPLETE,
    AUDIODEVICEADDED = SDL_AUDIODEVICEADDED,
    AUDIODEVICEREMOVED = SDL_AUDIODEVICEREMOVED,
    SENSORUPDATE = SDL_SENSORUPDATE,
    RENDER_TARGETS_RESET = SDL_RENDER_TARGETS_RESET,
    RENDER_DEVICE_RESET = SDL_RENDER_DEVICE_RESET,
    USEREVENT = SDL_USEREVENT,
    LASTEVENT = SDL_LASTEVENT,
};

enum Events::KeyCode : Flag::Type {
    UNKNOWN = SDLK_UNKNOWN,
    RETURN = SDLK_RETURN,
    ESCAPE = SDLK_ESCAPE,
    BACKSPACE = SDLK_BACKSPACE,
    TAB = SDLK_TAB,
    SPACE = SDLK_SPACE,
    EXCLAIM = SDLK_EXCLAIM,
    QUOTEDBL = SDLK_QUOTEDBL,
    HASH = SDLK_HASH,
    PERCENT = SDLK_PERCENT,
    DOLLAR = SDLK_DOLLAR,
    AMPERSAND = SDLK_AMPERSAND,
    QUOTE = SDLK_QUOTE,
    LEFTPAREN = SDLK_LEFTPAREN,
    RIGHTPAREN = SDLK_RIGHTPAREN,
    ASTERISK = SDLK_ASTERISK,
    PLUS = SDLK_PLUS,
    COMMA = SDLK_COMMA,
    MINUS = SDLK_MINUS,
    PERIOD = SDLK_PERIOD,
    SLASH = SDLK_SLASH,
    _0 = SDLK_0,
    _1 = SDLK_1,
    _2 = SDLK_2,
    _3 = SDLK_3,
    _4 = SDLK_4,
    _5 = SDLK_5,
    _6 = SDLK_6,
    _7 = SDLK_7,
    _8 = SDLK_8,
    _9 = SDLK_9,
    COLON = SDLK_COLON,
    SEMICOLON = SDLK_SEMICOLON,
    LESS = SDLK_LESS,
    EQUALS = SDLK_EQUALS,
    GREATER = SDLK_GREATER,
    QUESTION = SDLK_QUESTION,
    AT = SDLK_AT,
    LEFTBRACKET = SDLK_LEFTBRACKET,
    BACKSLASH = SDLK_BACKSLASH,
    RIGHTBRACKET = SDLK_RIGHTBRACKET,
    CARET = SDLK_CARET,
    UNDERSCORE = SDLK_UNDERSCORE,
    BACKQUOTE = SDLK_BACKQUOTE,
    a = SDLK_a,
    b = SDLK_b,
    c = SDLK_c,
    d = SDLK_d,
    e = SDLK_e,
    f = SDLK_f,
    g = SDLK_g,
    h = SDLK_h,
    i = SDLK_i,
    j = SDLK_j,
    k = SDLK_k,
    l = SDLK_l,
    m = SDLK_m,
    n = SDLK_n,
    o = SDLK_o,
    p = SDLK_p,
    q = SDLK_q,
    r = SDLK_r,
    s = SDLK_s,
    t = SDLK_t,
    u = SDLK_u,
    v = SDLK_v,
    w = SDLK_w,
    x = SDLK_x,
    y = SDLK_y,
    z = SDLK_z,
    CAPSLOCK = SDLK_CAPSLOCK,
    F1 = SDLK_F1,
    F2 = SDLK_F2,
    F3 = SDLK_F3,
    F4 = SDLK_F4,
    F5 = SDLK_F5,
    F6 = SDLK_F6,
    F7 = SDLK_F7,
    F8 = SDLK_F8,
    F9 = SDLK_F9,
    F10 = SDLK_F10,
    F11 = SDLK_F11,
    F12 = SDLK_F12,
    PRINTSCREEN = SDLK_PRINTSCREEN,
    SCROLLLOCK = SDLK_SCROLLLOCK,
    PAUSE = SDLK_PAUSE,
    INSERT = SDLK_INSERT,
    HOME = SDLK_HOME,
    PAGEUP = SDLK_PAGEUP,
    DELETE = SDLK_DELETE,
    END = SDLK_END,
    PAGEDOWN = SDLK_PAGEDOWN,
    RIGHT = SDLK_RIGHT,
    LEFT = SDLK_LEFT,
    DOWN = SDLK_DOWN,
    UP = SDLK_UP,
    NUMLOCKCLEAR = SDLK_NUMLOCKCLEAR,
    KP_DIVIDE = SDLK_KP_DIVIDE,
    KP_MULTIPLY = SDLK_KP_MULTIPLY,
    KP_MINUS = SDLK_KP_MINUS,
    KP_PLUS = SDLK_KP_PLUS,
    KP_ENTER = SDLK_KP_ENTER,
    KP_1 = SDLK_KP_1,
    KP_2 = SDLK_KP_2,
    KP_3 = SDLK_KP_3,
    KP_4 = SDLK_KP_4,
    KP_5 = SDLK_KP_5,
    KP_6 = SDLK_KP_6,
    KP_7 = SDLK_KP_7,
    KP_8 = SDLK_KP_8,
    KP_9 = SDLK_KP_9,
    KP_0 = SDLK_KP_0,
    KP_PERIOD = SDLK_KP_PERIOD,
    APPLICATION = SDLK_APPLICATION,
    POWER = SDLK_POWER,
    KP_EQUALS = SDLK_KP_EQUALS,
    F13 = SDLK_F13,
    F14 = SDLK_F14,
    F15 = SDLK_F15,
    F16 = SDLK_F16,
    F17 = SDLK_F17,
    F18 = SDLK_F18,
    F19 = SDLK_F19,
    F20 = SDLK_F20,
    F21 = SDLK_F21,
    F22 = SDLK_F22,
    F23 = SDLK_F23,
    F24 = SDLK_F24,
    EXECUTE = SDLK_EXECUTE,
    HELP = SDLK_HELP,
    MENU = SDLK_MENU,
    SELECT = SDLK_SELECT,
    STOP = SDLK_STOP,
    AGAIN = SDLK_AGAIN,
    UNDO = SDLK_UNDO,
    CUT = SDLK_CUT,
    COPY = SDLK_COPY,
    PASTE = SDLK_PASTE,
    FIND = SDLK_FIND,
    MUTE = SDLK_MUTE,
    VOLUMEUP = SDLK_VOLUMEUP,
    VOLUMEDOWN = SDLK_VOLUMEDOWN,
    KP_COMMA = SDLK_KP_COMMA,
    KP_EQUALSAS400 = SDLK_KP_EQUALSAS400,
    ALTERASE = SDLK_ALTERASE,
    SYSREQ = SDLK_SYSREQ,
    CANCEL = SDLK_CANCEL,
    CLEAR = SDLK_CLEAR,
    PRIOR = SDLK_PRIOR,
    RETURN2 = SDLK_RETURN2,
    SEPARATOR = SDLK_SEPARATOR,
    OUT = SDLK_OUT,
    OPER = SDLK_OPER,
    CLEARAGAIN = SDLK_CLEARAGAIN,
    CRSEL = SDLK_CRSEL,
    EXSEL = SDLK_EXSEL,
    KP_00 = SDLK_KP_00,
    KP_000 = SDLK_KP_000,
    THOUSANDSSEPARATOR = SDLK_THOUSANDSSEPARATOR,
    DECIMALSEPARATOR = SDLK_DECIMALSEPARATOR,
    CURRENCYUNIT = SDLK_CURRENCYUNIT,
    CURRENCYSUBUNIT = SDLK_CURRENCYSUBUNIT,
    KP_LEFTPAREN = SDLK_KP_LEFTPAREN,
    KP_RIGHTPAREN = SDLK_KP_RIGHTPAREN,
    KP_LEFTBRACE = SDLK_KP_LEFTBRACE,
    KP_RIGHTBRACE = SDLK_KP_RIGHTBRACE,
    KP_TAB = SDLK_KP_TAB,
    KP_BACKSPACE = SDLK_KP_BACKSPACE,
    KP_A = SDLK_KP_A,
    KP_B = SDLK_KP_B,
    KP_C = SDLK_KP_C,
    KP_D = SDLK_KP_D,
    KP_E = SDLK_KP_E,
    KP_F = SDLK_KP_F,
    KP_XOR = SDLK_KP_XOR,
    KP_POWER = SDLK_KP_POWER,
    KP_PERCENT = SDLK_KP_PERCENT,
    KP_LESS = SDLK_KP_LESS,
    KP_GREATER = SDLK_KP_GREATER,
    KP_AMPERSAND = SDLK_KP_AMPERSAND,
    KP_DBLAMPERSAND = SDLK_KP_DBLAMPERSAND,
    KP_VERTICALBAR = SDLK_KP_VERTICALBAR,
    KP_DBLVERTICALBAR = SDLK_KP_DBLVERTICALBAR,
    KP_COLON = SDLK_KP_COLON,
    KP_HASH = SDLK_KP_HASH,
    KP_SPACE = SDLK_KP_SPACE,
    KP_AT = SDLK_KP_AT,
    KP_EXCLAM = SDLK_KP_EXCLAM,
    KP_MEMSTORE = SDLK_KP_MEMSTORE,
    KP_MEMRECALL = SDLK_KP_MEMRECALL,
    KP_MEMCLEAR = SDLK_KP_MEMCLEAR,
    KP_MEMADD = SDLK_KP_MEMADD,
    KP_MEMSUBTRACT = SDLK_KP_MEMSUBTRACT,
    KP_MEMMULTIPLY = SDLK_KP_MEMMULTIPLY,
    KP_MEMDIVIDE = SDLK_KP_MEMDIVIDE,
    KP_PLUSMINUS = SDLK_KP_PLUSMINUS,
    KP_CLEAR = SDLK_KP_CLEAR,
    KP_CLEARENTRY = SDLK_KP_CLEARENTRY,
    KP_BINARY = SDLK_KP_BINARY,
    KP_OCTAL = SDLK_KP_OCTAL,
    KP_DECIMAL = SDLK_KP_DECIMAL,
    KP_HEXADECIMAL = SDLK_KP_HEXADECIMAL,
    LCTRL = SDLK_LCTRL,
    LSHIFT = SDLK_LSHIFT,
    LALT = SDLK_LALT,
    LGUI = SDLK_LGUI,
    RCTRL = SDLK_RCTRL,
    RSHIFT = SDLK_RSHIFT,
    RALT = SDLK_RALT,
    RGUI = SDLK_RGUI,
    MODE = SDLK_MODE,
    AUDIONEXT = SDLK_AUDIONEXT,
    AUDIOPREV = SDLK_AUDIOPREV,
    AUDIOSTOP = SDLK_AUDIOSTOP,
    AUDIOPLAY = SDLK_AUDIOPLAY,
    AUDIOMUTE = SDLK_AUDIOMUTE,
    MEDIASELECT = SDLK_MEDIASELECT,
    WWW = SDLK_WWW,
    MAIL = SDLK_MAIL,
    CALCULATOR = SDLK_CALCULATOR,
    COMPUTER = SDLK_COMPUTER,
    AC_SEARCH = SDLK_AC_SEARCH,
    AC_HOME = SDLK_AC_HOME,
    AC_BACK = SDLK_AC_BACK,
    AC_FORWARD = SDLK_AC_FORWARD,
    AC_STOP = SDLK_AC_STOP,
    AC_REFRESH = SDLK_AC_REFRESH,
    AC_BOOKMARKS = SDLK_AC_BOOKMARKS,
    BRIGHTNESSDOWN = SDLK_BRIGHTNESSDOWN,
    BRIGHTNESSUP = SDLK_BRIGHTNESSUP,
    DISPLAYSWITCH = SDLK_DISPLAYSWITCH,
    KBDILLUMTOGGLE = SDLK_KBDILLUMTOGGLE,
    KBDILLUMDOWN = SDLK_KBDILLUMDOWN,
    KBDILLUMUP = SDLK_KBDILLUMUP,
    EJECT = SDLK_EJECT,
    SLEEP = SDLK_SLEEP,
    APP1 = SDLK_APP1,
    APP2 = SDLK_APP2,
    AUDIOREWIND = SDLK_AUDIOREWIND,
    AUDIOFASTFORWARD = SDLK_AUDIOFASTFORWARD,
};

namespace Events {
constexpr auto convert(SDL_Event const & event) -> Event {
    switch (event.type) {
    case Type::QUIT:
        return Event{event.quit};
    case Type::DISPLAYEVENT:
        return Event{event.display};
    case Type::WINDOWEVENT:
        return Event{event.window};
    case Type::SYSWMEVENT:
        return Event{event.syswm};
    case Type::KEYDOWN:
        [[fallthrough]];
    case Type::KEYUP:
        return Event{event.key};
    case Type::TEXTEDITING:
        return Event{event.edit};
    case Type::TEXTINPUT:
        return Event{event.text};
    case Type::MOUSEMOTION:
        return Event{event.motion};
    case Type::MOUSEBUTTONDOWN:
        [[fallthrough]];
    case Type::MOUSEBUTTONUP:
        return Event{event.button};
    case Type::MOUSEWHEEL:
        return Event{event.wheel};
    case Type::JOYAXISMOTION:
        return Event{event.jaxis};
    case Type::JOYBALLMOTION:
        return Event{event.jball};
    case Type::JOYHATMOTION:
        return Event{event.jhat};
    case Type::JOYBUTTONDOWN:
        [[fallthrough]];
    case Type::JOYBUTTONUP:
        return Event{event.jbutton};
    case Type::JOYDEVICEADDED:
        [[fallthrough]];
    case Type::JOYDEVICEREMOVED:
        return Event{event.jdevice};
    case Type::CONTROLLERAXISMOTION:
        return Event{event.caxis};
    case Type::CONTROLLERBUTTONDOWN:
        [[fallthrough]];
    case Type::CONTROLLERBUTTONUP:
        return Event{event.cbutton};
    case Type::CONTROLLERDEVICEADDED:
        [[fallthrough]];
    case Type::CONTROLLERDEVICEREMOVED:
        [[fallthrough]];
    case Type::CONTROLLERDEVICEREMAPPED:
        return Event{event.cdevice};
#ifdef SDL_CONTROLLERTOUCHPADDOWN
    case Type::CONTROLLERTOUCHPADDOWN:
        return Event{event.ctouchpad};
#endif // SDL_CONTROLLERTOUCHPADDOWN
#ifdef SDL_CONTROLLERTOUCHPADMOTION
    case Type::CONTROLLERTOUCHPADMOTION:
        return Event{event.ctouchpad};
#endif // SDL_CONTROLLERTOUCHPADMOTION
#ifdef SDL_CONTROLLERTOUCHPADUP
    case Type::CONTROLLERTOUCHPADUP:
        return Event{event.ctouchpad};
#endif // SDL_CONTROLLERTOUCHPADUP
#ifdef SDL_CONTROLLERSENSORUPDATE
    case Type::CONTROLLERSENSORUPDATE:
        return Event{event.csensor};
#endif // SDL_CONTROLLERSENSORUPDATE
    case Type::FINGERDOWN:
        [[fallthrough]];
    case Type::FINGERUP:
        [[fallthrough]];
    case Type::FINGERMOTION:
        return Event{event.tfinger};
    case Type::DOLLARGESTURE:
        [[fallthrough]];
    case Type::DOLLARRECORD:
        return Event{event.dgesture};
    case Type::MULTIGESTURE:
        return Event{event.mgesture};
    case Type::DROPFILE:
        [[fallthrough]];
    case Type::DROPTEXT:
        [[fallthrough]];
    case Type::DROPBEGIN:
        [[fallthrough]];
    case Type::DROPCOMPLETE:
        return Event{event.drop};
    case Type::AUDIODEVICEADDED:
        [[fallthrough]];
    case Type::AUDIODEVICEREMOVED:
        return Event{event.adevice};
    case Type::SENSORUPDATE:
        return Event{event.sensor};
    case Type::USEREVENT:
        [[fallthrough]];
    case Type::LASTEVENT:
        return Event{event.user};
    case Type::FIRSTEVENT:
        [[fallthrough]];
    case Type::APP_TERMINATING:
        [[fallthrough]];
    case Type::APP_LOWMEMORY:
        [[fallthrough]];
    case Type::APP_WILLENTERBACKGROUND:
        [[fallthrough]];
    case Type::APP_DIDENTERBACKGROUND:
        [[fallthrough]];
    case Type::APP_WILLENTERFOREGROUND:
        [[fallthrough]];
    case Type::APP_DIDENTERFOREGROUND:
        return Event{event.common};
#ifdef SDL_LOCALECHANGED
    case Type::LOCALECHANGED:
        return Event{event.common};
#endif // SDL_LOCALECHANGED
    case Type::KEYMAPCHANGED:
        [[fallthrough]];
    case Type::CLIPBOARDUPDATE:
        [[fallthrough]];
    case Type::RENDER_TARGETS_RESET:
        [[fallthrough]];
    case Type::RENDER_DEVICE_RESET:
        return Event{event.common};
    default:
        throw UnknownEvent{event.type};
    }
}
} // namespace Events

} // namespace SDL

#endif // SDL2PP_EVENT
