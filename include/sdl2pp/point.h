#ifndef SDL2PP_POINT
#define SDL2PP_POINT

#include <SDL_rect.h>

namespace SDL {
using Point = SDL_Point;

constexpr auto equals(Point const & first, Point const & second) -> bool {
    return first.x == second.x && first.y == second.y;
}

} // namespace SDL

#endif
