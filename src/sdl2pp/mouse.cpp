#include "sdl2pp/mouse.h"

#include <SDL_mouse.h>

#include <glm/glm.hpp>

#include <bitset>

namespace SDL {

auto Mouse::state() -> State {
    auto position = glm::ivec2{};
    auto const buttons = SDL_GetMouseState(&position.x, &position.y);
    return State{.position = position, .buttons = buttons};
}

} // namespace SDL
