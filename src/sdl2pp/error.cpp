#include "error.h"

#include <SDL_error.h>

namespace SDL {

Error::Error()
: std::runtime_error(SDL_GetError()){};

} // namespace SDL
