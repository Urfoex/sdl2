#include "window.h"
#include "error.h"

#include <spdlog/spdlog.h>

namespace SDL {

auto Window::WindowDeleter::operator()(SDL_Window * const window) -> void {
    spdlog::info("SDL::Window Destroy");
    SDL_DestroyWindow(window);
}

Window::Window(Args const & args)
: window{
    [&args]() -> SDL_Window * {
        spdlog::info("SDL::Window");
        return Error::validate<SDL_Window *>(
            SDL_CreateWindow(
                args.title.data(), args.x, args.y, args.w, args.h, args.flags),
            Error::isNullptr<SDL_Window>);
    }(),
    WindowDeleter{}} {
}

auto Window::createRenderer(Renderer::Args args) -> Renderer {
    return Renderer(Renderer::Args_{
        args,
        {this->window.get()},
    });
}

auto Window::createSharedRenderer(Renderer::Args args)
    -> std::shared_ptr<Renderer> {
    return std::make_shared<Renderer>(Renderer::Args_{
        args,
        {this->window.get()},
    });
}

} // namespace SDL
