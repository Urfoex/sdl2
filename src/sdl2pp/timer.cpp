#include "timer.h"

#include <SDL_timer.h>

namespace SDL {
namespace Timer {

auto delay(std::chrono::milliseconds const ms) -> void {
    SDL_Delay(static_cast<uint32_t>(ms.count()));
}

} // namespace Timer
} // namespace SDL
