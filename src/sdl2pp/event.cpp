#include "event.h"

#include <fmt/format.h>

namespace SDL {
auto EventHandler::poll() -> bool {
    if (SDL_PollEvent(&this->_event) != 0) {
        this->event = Events::convert(this->_event);
        return true;
    }
    return false;
}

UnknownEvent::UnknownEvent(uint32_t const eventType)
: std::runtime_error{fmt::format("Unknown Event: {}", eventType)} {
}

} // namespace SDL
