#include "sdl2pp.h"
#include "error.h"

#include <SDL.h>
#include <spdlog/spdlog.h>

#include <memory>
#include <optional>

namespace SDL {

SDL::SDL() {
    spdlog::info("SDL::Init");
    Error::validate<uint32_t>(
        SDL_Init(Init::Flags::TIMER | Init::Flags::AUDIO | Init::Flags::VIDEO
                 | Init::Flags::JOYSTICK | Init::Flags::GAMECONTROLLER
                 | Init::Flags::EVENTS),
        Error::isNotZero<uint32_t>);
}

SDL::~SDL() {
    spdlog::info("SDL::Quit");
    SDL_Quit();
}

auto SDL::wasInit(Init::Flags flags) -> bool {
    return SDL_WasInit(flags) != 0U;
}

auto SDL::quitSubSystem(Init::Flags flags) -> void {
    SDL_QuitSubSystem(flags);
}

auto SDL::initSubSystem(Init::Flags flags) -> void {
    Error::validate<uint32_t>(SDL_InitSubSystem(flags),
                              Error::isNotZero<uint32_t>);
}

} // namespace SDL
