#include "texture.h"
#include "error.h"

#include <SDL_render.h>
#include <spdlog/spdlog.h>

namespace SDL {

Texture::TextureDeleter::TextureDeleter(bool destroy)
: _destroy{destroy} {
}

auto Texture::TextureDeleter::operator()(SDL_Texture * const texture) const
    -> void {
    if (this->_destroy) {
        // spdlog::info("SDL::Texture Destroy: {}",
        //         static_cast<void *>(texture));
        SDL_DestroyTexture(texture);
    }
}

Texture::Texture(Of const & args)
: texture{args.texture, TextureDeleter{false}} {
    // spdlog::info("SDL::Texture Of: {}",
    //              static_cast<void *>(this->texture.get()));
}

Texture::Texture(Args_ const & args)
: texture{
    [&args]() {
        return Error::validate<SDL_Texture *>(
            SDL_CreateTexture(args.renderer,
                              args.pixelFormat, // SDL_PIXELFORMAT_RGBA8888,
                              args.access,      // SDL_TEXTUREACCESS_TARGET,
                              args.w,
                              args.h),
            Error::isNullptr<SDL_Texture>);
    }(),
    TextureDeleter{true}} {
    // spdlog::info("SDL::Texture Create: {}",
    //              static_cast<void *>(this->texture.get()));
}

auto Texture::get() const -> SDL_Texture * {
    return this->texture.get();
};

// extern DECLSPEC int SDL_QueryTexture(SDL_Texture * texture,
//                                              uint32_t * format, int *access,
//                                              int *w, int *h);
//
// extern DECLSPEC int SDL_SetTextureColorMod(SDL_Texture * texture,
//                                                    uint8_t r, uint8_t g,
//                                                    uint8_t b);
//
//
// extern DECLSPEC int SDL_GetTextureColorMod(SDL_Texture * texture,
//                                                    uint8_t * r, uint8_t * g,
//                                                    uint8_t * b);
//
// extern DECLSPEC int SDL_SetTextureAlphaMod(SDL_Texture * texture,
//                                                    uint8_t alpha);
//
// extern DECLSPEC int SDL_GetTextureAlphaMod(SDL_Texture * texture,
//                                                    uint8_t * alpha);
//
// extern DECLSPEC int SDL_SetTextureBlendMode(SDL_Texture * texture,
//                                                     SDL_BlendMode blendMode);
//
// extern DECLSPEC int SDL_GetTextureBlendMode(SDL_Texture * texture,
//                                                     SDL_BlendMode
//                                                     *blendMode);
//
// extern DECLSPEC int SDL_UpdateTexture(SDL_Texture * texture,
//                                               const SDL_Rect * rect,
//                                               const void *pixels, int pitch);
//
// extern DECLSPEC int SDL_UpdateYUVTexture(SDL_Texture * texture,
//                                                  const SDL_Rect * rect,
//                                                  const uint8_t *Yplane, int
//                                                  Ypitch, const uint8_t
//                                                  *Uplane, int Upitch, const
//                                                  uint8_t *Vplane, int
//                                                  Vpitch);
//
// extern DECLSPEC int SDL_LockTexture(SDL_Texture * texture,
//                                             const SDL_Rect * rect,
//                                             void **pixels, int *pitch);
//
// extern DECLSPEC void SDL_UnlockTexture(SDL_Texture * texture);
//
// extern DECLSPEC int SDL_GL_BindTexture(SDL_Texture *texture, float *texw,
// float *texh);
//
// extern DECLSPEC int SDL_GL_UnbindTexture(SDL_Texture *texture);

} // namespace SDL
