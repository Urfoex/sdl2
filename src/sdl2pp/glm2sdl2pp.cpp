#include "glm2sdl2pp.h"

#include <glm/glm.hpp>

#include <SDL_rect.h>

namespace GLM {

auto toPoint(glm::dvec2 const & vec) -> SDL::Point {
    return SDL::Point{.x = static_cast<int>(std::nearbyint(vec.x)),
                      .y = static_cast<int>(std::nearbyint(vec.y))};
}

auto toPoints(std::vector<glm::dvec2> const & points)
    -> std::vector<SDL::Point> {
    auto convertedPoints = std::vector<SDL::Point>{};
    for (auto const & glmVec : points) {
        auto const sdlPoint = toPoint(glmVec);
        if (convertedPoints.empty()
            || (!SDL::equals(convertedPoints.rbegin()[0], sdlPoint))) {
            convertedPoints.push_back(sdlPoint);
        }
    }
    return convertedPoints;
}

} // namespace GLM
