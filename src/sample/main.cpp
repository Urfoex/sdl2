#include "sdl2pp/error.h"
#include "sdl2pp/event.h"
#include "sdl2pp/pixel.h"
#include "sdl2pp/renderer.h"
#include "sdl2pp/sdl2pp.h"
#include "sdl2pp/texture.h"
#include "sdl2pp/visitors.h"
#include "sdl2pp/window.h"
#include "sdl2pp/with.h"

#include <spdlog/spdlog.h>

#include <cstdint>
#include <utility>
#include <variant>

auto main([[maybe_unused]] int argc, [[maybe_unused]] char ** argv) -> int {
    try {
        with(SDL::SDL{})([](auto &&...) {
            auto window = SDL::Window(
                SDL::Window::Args{.title = "SDL::Test",
                                  .x = SDL::Window::Pos::CENTERED,
                                  .y = SDL::Window::Pos::CENTERED,
                                  .w = 320,
                                  .h = 240,
                                  .flags = SDL::Window::Flags::OPENGL
                                           | SDL::Window::Flags::BORDERLESS});
            auto renderer = window.createRenderer(SDL::Renderer::Args{
                .flags = SDL::Renderer::Flags::ACCELERATED
                         | SDL::Renderer::Flags::TARGETTEXTURE
                         | SDL::Renderer::Flags::PRESENTVSYNC

            });

            auto running = true;
            auto eventHandler = SDL::EventHandler{};
            auto r = uint8_t{0};
            auto g = uint8_t{0};
            auto b = uint8_t{0};

            auto const visitors = Visitors{
                [&running]([[maybe_unused]] SDL::Events::Quit const & event) {
                    running = false;
                },
                [&running](SDL::Events::Keyboard const & event) {
                    if (event.state == SDL::Events::PRESSED) {
                        switch (event.keysym.sym) {
                        case SDL::Events::KeyCode::ESCAPE:
                            running = false;
                            break;
                        }
                    }
                },
                []([[maybe_unused]] auto const & event) {}};

            auto renderTarget = renderer.getTarget();
            auto sampleTexture = renderer.createTexture(
                SDL::Texture::Args{.pixelFormat = SDL::Pixel::Format::RGBA8888,
                                   .access = SDL::Texture::Access::STATIC,
                                   .w = 32,
                                   .h = 32});
            while (running) {
                renderer.setDrawColor(SDL::Color{r++, g, b, 255});
                {
                    g += r % 255;
                    b += b % 255;
                }
                renderer.clear();

                eventHandler.handleEvents(visitors);

                renderer.present();
            }
        });
    } catch (SDL::Error const & error) {
        spdlog::error("Exception: {}", error.what());
    }
    return 0;
}
