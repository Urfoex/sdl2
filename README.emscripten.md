# Building with emscripten

## Command

```
mkdir build_emscripten
cd build_emscripten
emcmake cmake \
    -Dglm_DIR=${PATH/TO/sdl2}/CMakeConfigs/ \
    -Dglm_INCLUDE_DIRS=${PATH/TO/glm}/ \
    -Dglm_LIBRARIES=${PATH/TO/glm}/build_emscripten/glm/libglm_static.a \
    -Dfmt_DIR=${PATH/TO/sdl2}/CMakeConfigs/ \
    -Dfmt_INCLUDE_DIRS=${PATH/TO/fmt}/include/ \
    -Dfmt_LIBRARIES=${PATH/TO/fmt}/build_emscripten/libfmt.a \
    -Dspdlog_DIR=${PATH/TO/sdl2}/CMakeConfigs/ \
    -Dspdlog_INCLUDE_DIRS=${PATH/TO/spdlog}/include/ \
    -Dspdlog_LIBRARIES=${PATH/TO/spdlog}/build_emscripten/libspdlog.a \
    -DSDL2_DIR=${PATH/TO/sdl2}/CMakeConfigs/ \
    -GNinja \
    ..
ninja
```

## Getting emscripten

```
git clone https://github.com/emscripten-core/emsdk.git
./emsdk install latest
./emsdk activate latest
source "${CURRENT/PATH}/emsdk_env.sh"
```

## Getting libraries for emscripten

### fmt

```
git clone https://github.com/fmtlib/fmt.git
mkdir build_emscripten
cd build_emscripten
emcmake cmake -GNinja ..
ninja
```

### spdlog

```
git clone https://github.com/gabime/spdlog/
mkdir build_emscripten
cd build_emscripten
emcmake cmake -GNinja ..
ninja
```

### glm

```
git clone https://github.com/g-truc/glm
mkdir build_emscripten
cd build_emscripten
emcmake cmake -GNinja ..
ninja
```

<!---
vim: conceallevel=0
-->
